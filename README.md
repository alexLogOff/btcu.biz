# cryptowidget

Криптовалютный виджет

Позволяет пользователю разместить на своем личном сайте виджет для оплаты товаров
посетителями через украинские терминалы или используя кредитные карты, используя
технология сайта https://btcu.biz/
Сумма с кода зачисляется по выбору: на BTC адрес (владельца магазина), KUNA код или EXMO код - для них отдельно дописать функционал чтоб погашать на свой счет биржи или просто складывать отдельно и погашать вручную


Интеграция виджета:
  
   1. Добавить на странице

    <div id="modal">
      <btcu-widget></btcu-widget>
    </div>
    <script src="/dist/widget.js"></script>
    <button id="button">Click For Btcu code modal</button>

   2. входящие параметры виджета
      amount - сумма заказа (обязательный параметр, добавляется скриптом магазина)
      order_id - номер заказа (обязательный параметр, добавляется скриптом магазина)
      currency - тип валюты (по умолчанию UAH)
      url - путь к обработкику (по умозчанию /index.php)
      button - id кнопки запуска виджета (по умолчанию button)
  
    <btcu-widget :amount="10" :order_id="101" :currency="UAH" :url="https:://my_site.com/some_path/Controller/widjet" :button="confirm-buttom" ></btcu-widget>

   3. добавить свой функционал с установкой order_id, суммы необходимого платежа (если сумма в чеке меньше то будет выдано сообщение "Сумма чека меньше суммы к оплате"), выбора способа погашения платежа (BTC, EXMO, KUNA)
