<?php


namespace BTCU;


class Client
{

  const URL = 'btcu.biz';

  const CHECK_BTCU_CODE_URL  = 'https://'.self::URL.'/api/btcu/code/check';
  const SEND_VERIFY_SMS_URL  = 'https://'.self::URL.'/api/btcu/code/send-verify-sms';
  const REPAY_TO_BTC_URL     = 'https://'.self::URL.'/api/btcu/code/repay/btc';
  const REPAY_TO_EXMO_URL    = 'https://'.self::URL.'/api/btcu/code/repay/exmo';
  const REPAY_TO_KUNA_URL    = 'https://'.self::URL.'/api/btcu/code/repay/kuna';

  public $cookie = [];

  /**
   * Проверка кода на существование, получение суммы кода
   *
   * @param $btcuCode
   * @param $client_info
   * @return bool|string
   */
  public function checkBtcuCode($btcuCode, $client_info) {
      $data = [
              'client_info'  => $client_info,
              'code'         => $btcuCode,
              'disclaimer'   => true,
              'terms'        => true
            ];
      $response = $this->request(self::CHECK_BTCU_CODE_URL, $data);
      return json_decode($response, true);
  }

  /**
   * Отправка запроса на получение СМС кода верификации платежа
   *
   * @param $client_info
   * @return bool|string
   */
  public function sendVerifySms($client_info) {
    $data = [
      'client_info'  => $client_info,
    ];
    return $this->request(self::SEND_VERIFY_SMS_URL, $data);
  }

  /**
   * Погашение кода на BTC адрес
   *
   * @param $btcuCode
   * @param $smsCode
   * @param $client_info
   * @param $address
   * @return bool|string
   */
  public function repayToBTC($btcuCode, $smsCode, $client_info, $address){
      $data = [
          'client_info'      => $client_info,
          'code'             => $btcuCode,
          'sms_code'         => $smsCode,
          'address'          => $address,
          'confirm_send_btc' => true
      ];
      return $this->request(self::REPAY_TO_BTC_URL, $data);
  }

  /**
   * Погашение на ЭКСМО купон
   *
   * @param $btcuCode
   * @param $smsCode
   * @param $client_info
   * @return bool|string
   */
  public function repayToEXMO($btcuCode, $smsCode, $client_info){
    $data = [
      'client_info'      => $client_info,
      'code'             => $btcuCode,
      'sms_code'         => $smsCode,
    ];
    return $this->request(self::REPAY_TO_EXMO_URL, $data);
  }

  /**
   * Погашение на KUNA купон
   *
   * @param $btcuCode
   * @param $smsCode
   * @param $client_info
   * @return bool|string
   */
  public function repayToKUNA($btcuCode, $smsCode, $client_info){
    $data = [
      'client_info'      => $client_info,
      'code'             => $btcuCode,
      'sms_code'         => $smsCode,
    ];
    return $this->request(self::REPAY_TO_KUNA_URL, $data);
  }

  /**
   * @param $url
   * @param $data
   * @return bool|string
   */
  private function request($url, $data = [])
  {
      $data_string = json_encode($data);
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_HEADER, 1);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_COOKIE, 'btcu_session=' . $this->getCookie());
      curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string)]
      );
      $response = curl_exec($ch);
      $parts = explode("\r\n\r\nHTTP/", $response);
      $parts = (count($parts) > 1 ? 'HTTP/' : '').array_pop($parts);
      list($headers, $body) = explode("\r\n\r\n", $parts, 2);
      $this->setCookie($headers);

      // получение ответа, парсинг
      $_response = json_decode($body, true);

      // если в ответе есть упоминание ошибки - отображаем ошибку
      if (isset($_response) && array_key_exists('errors',$_response)) {
          http_response_code(422);
          die($body);
      }
      return $body;
  }

  /**
   * Запоминаем сессию для корректного проведения серии запросов
   *
   * @return string
   */
  private function getCookie()
  {
    if (array_key_exists('btcu_session', $this->cookie))
    {
      return $this->cookie['btcu_session'];
    }
    return '';
  }

  /**
   * @param $response
   */
  private function setCookie($response){
    preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response, $matches);
    foreach($matches[1] as $item) {
      parse_str($item, $cookie);
      $this->cookie = $this->cookie ? array_merge($this->cookie, $cookie) : $cookie;
    }
  }
}

/**
 * @var $amount int (UAH)
 */
$amount = 1000;

/**
 * Номер заказа (внутренний, для коррек)
 * @var $order_id string|int
 */
$order_id = 1213151;

/**
 * Номер BTC кошелька для погашения кода на BTC адресс
 */
$address = '2MwLCiCSnN1Zoeht21VnxGJAQeHV6nKkXGS';

$client = new Client();
$post = json_decode(file_get_contents('php://input'), true);

  if (isset($post['btcu'], $post['client_info']) && !isset($post['sms']))
  {
      // Запрос на проверку кода, проверяем что введенный посетителем код существует и получаем его стоимость
      $response = $client->checkBtcuCode($post['btcu'], $post['client_info']);

      // корректный ответ имеет вид:
      // response
      // [
      //    'amount_uah'    => '',
      //    'amount_btc'    => '',
      //    'amount_usd'    => ''
      // ];

      // Проверяем пришла ли сумма чека и совпадает ли с суммой заказа
      if(!isset($response['amount_uah'])){
        $error =  json_encode(['errors'=> ['Сервис временно недоступен']]);
        http_response_code(422);
        die($error);
      }
      if($amount > ($response['amount_uah'] / 1e8))
      {
        $error =  json_encode(['errors'=> ['Сумма чека меньше суммы к оплате']]);
        http_response_code(422);
        die($error);
      }
      // если всё ок делаем запрос на отправку смс
      $response = $client->sendVerifySms($post['client_info']);
      //
      //  [
      //    'isCheckSmsCode' => true,
      //    'mess' => ''
      //  ]
      die($response);
  }

  if (isset($post['btcu'], $post['sms'], $post['client_info']))
  {
        //Гасим чек на btc адресс (или код EXMO или KUNA)
        $response = $client->repayToBTC($post['btcu'], $post['sms'],$post['client_info'], $address);
        // response
        // [
        //   'btc_fee' => 10000,
        // ];

         // если EXMO или KUNA не забываем сохранить купон для отдельного погашения на свой баланс в бирже
         // [
         //   'exmo_coupon'   => '',
         //   'amount'        => '',
         //   'cost'          => '',
         // ]

        die($response);
  }

include 'simple.html';
